import httpx
from bs4 import BeautifulSoup

from config import config


async def get_login_async(async_client: httpx.AsyncClient) -> httpx.Response:
    """
    Perform a GET request to the login page
    :param async_client: async httpx client
    :return: Response object
    """
    login_url = f'https://{config["DOMAIN_NAME"]}/login'
    res = await async_client.get(login_url, follow_redirects=True)
    res.raise_for_status()
    return res


def extract_csrf_from_login_page(res: httpx.Response) -> str:
    """
    Extract csrf from login page
    :param res: Response object
    :return: CSRF string
    """
    soup = BeautifulSoup(res.text, 'html.parser')
    form = soup.find('form', {'class': 'login-form'})
    csrf_elem = form.find('input', {'name': 'csrf'})
    csrf = csrf_elem.get('value')
    return csrf


async def post_login_async(username: str,
                           password: str,
                           csrf: str,
                           async_client: httpx.AsyncClient = None) -> httpx.Response:
    """
    Perform a POST request to the login page
    :param username: username
    :param password: password
    :param csrf: CSRF
    :param async_client: async httpx client
    :return: Response object
    """
    if async_client is None:
        async with httpx.AsyncClient() as client:
            return await post_login_async(username, password, csrf, async_client=client)
    login_url = f'https://{config["DOMAIN_NAME"]}/login'
    data = {
        'username': username,
        'password': password,
        'csrf':     csrf,
    }
    res = await async_client.post(login_url, data=data, follow_redirects=False)
    return res


def extract_csrf_from_two_fa_page(res_str: str) -> str:
    """
    Extract csrf from 2FA page
    :param res_str: response text
    :return: CSRF string
    """
    soup = BeautifulSoup(res_str, 'html.parser')
    form = soup.find('form', {'class': 'login-form'})
    csrf_elem = form.find('input', {'name': 'csrf'})
    csrf = csrf_elem.get('value')
    return csrf


async def post_two_fa_async(token: str,
                            csrf: str,
                            two_fa_url: str = '',
                            async_client: httpx.AsyncClient = None) -> httpx.Response:
    """
    Perform a POST request to 2FA endpoint
    :param token: 2FA token
    :param csrf: CSRF
    :param two_fa_url: 2FA endpoint url (if differ from the default)
    :param async_client: async httpx client
    :return: Response object
    """
    if async_client is None:
        async with httpx.AsyncClient() as client:
            return await post_two_fa_async(token, csrf, async_client=client)
    if two_fa_url == '':
        two_fa_url = f'https://{config["DOMAIN_NAME"]}/login2'
    data = {
        'mfa-code': token,
        'csrf': csrf,
    }
    res = await async_client.post(two_fa_url, data=data)
    return res
