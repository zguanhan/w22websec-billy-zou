# Websec 1.4 - Homework 1

### Install dependencies

- bs4
- httpx
- python-dotenv

```shell
pip install -r requirements.txt
```

### Method 1 - command line argument

```shell
python3 hw1.py https://ac031f861f998ba8c08e6a11003c002e.web-security-academy.net/
```

### Method 2 - .env file

Create `.env`:

```dotenv
DOMAIN_NAME=ac031f861f998ba8c08e6a11003c002e.web-security-academy.net
```