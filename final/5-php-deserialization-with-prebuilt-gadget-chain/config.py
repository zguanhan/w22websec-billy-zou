import re

def extract_domain_name(s):
    if s is None:
        return None
    regex_str = r'https?://(?P<domain_name>[A-Za-z_0-9.-]+).*'
    r = re.compile(regex_str)
    m = r.match(s)
    if m is None:
        return None
    dm = m.groupdict().get('domain_name')
    if dm is None:
        return None
    return dm

try:
    from dotenv import dotenv_values
    config = dotenv_values('.env')
    domain_name = extract_domain_name(config['LEVEL_DOMAIN_NAME'])
    if domain_name is not None:
        config['LEVEL_DOMAIN_NAME'] = domain_name
except ModuleNotFoundError as exc:
    config = dict()
