# CS495 Winter 2022 Notebook - Billy Zou (zguanhan)


## Changelog

### Final screencast (03-13-2022)

- `final/screencast_url.txt` - uploaded

### Lab 5 (03-06-2022)

- `notebooks/5.pdf` - completed (5.1-5.6)

### Lab 4 (03-05-2022)

- `notebooks/4.pdf` - maybe completed (4.1-4.7)

### Lab 3 (02-20-2022)

- `notebooks/3.pdf` - completed (3.1-3.7)


## Changelog

### Lab 2.1 (02-05-2022)

- `notebooks/2.pdf` - completed (2.1)

### Homework 2 (01-27-2022)

- `hw2` - submission

### Lab 1.5 1.6 1.7 (01-22-2022)

- `notebooks/1.pdf` - completed (1.5 1.6 1.7)

### Lab 1.3 (01-16-2022)

- `notebooks/1.pdf` - in progress (1.3)

### Homework 1 (01-05-2022)

- `hw1` - submission

### Lab 1.1 1.2 (01-03-2022)

- `notebooks/1.pdf` - in progress (1.1 1.2)

