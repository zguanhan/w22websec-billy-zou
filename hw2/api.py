import re

from bs4 import BeautifulSoup


def html_has_welcome_back(text: str) -> bool:
    """
    Find out if the HTML contains Welcome back message
    :param text: HTML response text
    :return: True if the page has Welcome back message, false otherwise
    """
    soup = BeautifulSoup(text, 'html.parser')
    top_links_elem = soup.find('section', {'class': 'top-links'})
    has_welcome_back = top_links_elem.find('div', text=re.compile('Welcome back!')) is not None
    return has_welcome_back
