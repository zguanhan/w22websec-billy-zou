import re
import argparse
import sys


def _config_from_dotenv():
    """
    :return: configuration dict
    """
    from dotenv import dotenv_values
    config = dotenv_values('.env')
    return config


def _config_from_argparser():
    """
    :return: configuration dict
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('domain_name', nargs='?', type=str, default='')
    args = parser.parse_args()
    return {
        'DOMAIN_NAME': args.domain_name,
    }


def _parse_domain_name(text: str) -> str:
    """
    Extracts domain name from the url
    :param text: url or url fragment
    :return: domain name
    """
    REGEX = re.compile(r'(?:https?://)?(?P<domain_name>(?:(?:[a-zA-Z0-9_-]+\.)*[a-zA-Z0-9_-]+))')
    matches = REGEX.search(text)
    if matches is None:
        raise ValueError(f'invalid url {text} in configuration')
    mapping = matches.groups()
    if len(mapping) < 1:
        raise ValueError(f'invalid url {text} in configuration')
    return mapping[0]


_args = _config_from_argparser()
config = {}
if _args['DOMAIN_NAME'] != '':
    config['DOMAIN_NAME'] = _args['DOMAIN_NAME']
else:
    try:
        config = _config_from_dotenv()
    except (ImportError, FileNotFoundError):
        print('Usage: python <script_name> <domain_name>\n\nEx python hw2.py https://xxx')
        sys.exit(1)
config['DOMAIN_NAME'] = _parse_domain_name(config['DOMAIN_NAME'])
domain_name = config['DOMAIN_NAME']